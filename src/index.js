const date = require('./date');
const number = require('./number');
const string = require('./string');

module.exports = {
  date,
  number,
  string,
};
