const Joi = require('@hapi/joi');

module.exports = Joi.string()
  .trim()
  .min(3)
  .max(1000);
