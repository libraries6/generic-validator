const positive = require('./positive');
const integer = require('./integer');
const precision = require('./precision');

module.exports = {
  integer,
  positive,
  precision,
};
