const Joi = require('@hapi/joi');

module.exports = Joi.number()
  .integer()
  .positive()
  .error((errs) => {
    const newErrs = errs.map((err) => {
      const newErr = err;
      switch (err.type) {
        case 'number.base':
          newErr.message = `${err.context.label} should be a number`;
          break;
        case 'number.integer':
          newErr.message = `${err.context.label} must be an integer`;
          break;
        case 'number.positive':
          newErr.message = `${err.context.label} can't be negative`;
          break;
        default:
          break;
      }
      return newErr;
    });
    return newErrs;
  });
