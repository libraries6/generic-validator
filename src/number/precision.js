const Joi = require('@hapi/joi');

module.exports = Joi.number()
  .precision(3);
