const validations = require('./src');

module.exports = {
  ...validations,
};
