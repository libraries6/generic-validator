# @abslibs/generic-validator

<!--[![NPM](https://nodei.co/npm/ngx-flyout.png)](https://www.npmjs.com/package/ngx-flyout)-->

Generic validator built on top of Joi

## Installation

```shell
npm i @abslibs/generic-validator --save
```

## Usage

```javascript
// require the entire library
const genericvalidator = require('@abslibs/generic-validation');

// or require individual components
const { string, number, date } = require('@abslibs/generic-validator');

// using a validation to validate something
const { error, result } = genericvalidator.number.positive.validate(1); // Success
const { error, result } = genericvalidator.number.positive.validate('abc'); // Error
```

## Available validator

### date

| Validation  | Usage                                                 | Description                                              |
| ----------- | ----------------------------------------------------- | -------------------------------------------------------- |
| timestamp   | date.timestamp.validate('2019-04-11T11:46:22.560Z')   | Check for a valid iso date time string                   |
| iso         | date.iso.validate('2019-04-19T09:09:40.826Z')         | Check for a valid iso date time string                   |
| isoWithNull | date.isoWithNull.validate('2019-04-19T09:09:40.826Z') | Check for a valid iso date time string. Allow null value |

### number

| Validation | Usage                            | Description                                            |
| ---------- | -------------------------------- | ------------------------------------------------------ |
| integer    | number.integer.validate('123')   | Check for a valid integer number                       |
| positive   | number.positive.validate('123')  | Check for a valid positive integer number              |
| precision  | number.precision.validate('123') | Check for a valid number with precision upto 3 decimal |

### string

| Validation | Usage                                              | Description                                      |
| ---------- | -------------------------------------------------- | ------------------------------------------------ |
| small      | string.small.validate('2019-04-11T11:46:22.560Z')  | Check for a string with length between 3 to 255  |
| medium     | string.medium.validate('2019-04-11T11:46:22.560Z') | Check for a string with length between 3 to 1000 |

## Changelog

See the [changelog page](https://gitlab.com/abslibs/node/generic-validator/blob/master/CHANGELOG.md) on Gitlab.
