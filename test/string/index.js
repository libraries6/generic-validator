const chai = require('chai');

const { expect } = chai;
const string = require('../../src/string');

let response;

describe('string test cases', () => {
  it('it should validate small string', () => {
    response = string.small.validate('test');
    expect(response.error).to.be.null;
  });

  it('it should not validate small string', () => {
    response = string.small.validate('when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum');
    expect(response.error).not.to.be.null;
  });

  it('it should validate medium string', () => {
    response = string.medium.validate('mediumm');
    expect(response.error).to.be.null;
  });

  it('it should not validate medium string', () => {
    response = string.medium.validate('a');
    expect(response.error).not.to.be.null;
  });
});
