const date = require('./date');
const string = require('./string');
const number = require('./number');

module.exports = {
  date,
  string,
  number,
};
