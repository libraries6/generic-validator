const chai = require('chai');

const { expect } = chai;
const number = require('./../../src/number');

let response;

describe('Number validations', () => {
  it('should validate integer values', () => {
    response = number.integer.validate(4);
    expect(response.error).be.null;
  });

  it('should not validate integer values', () => {
    response = number.integer.validate(4.45);
    expect(response.error).not.to.be.null;
  });

  it('should validate positive values', () => {
    response = number.positive.validate(4);
    expect(response.error).be.null;
  });

  it('should not validate positive values', () => {
    response = number.positive.validate(-4);
    expect(response.error).not.to.be.null;
  });

  it('should validate precision values', () => {
    response = number.precision.validate(1.12345);
    expect(response.error).be.null;
  });

  it('should not validate precision values for null', () => {
    response = number.precision.validate(null);
    expect(response.error).not.to.be.null;
  });
});
