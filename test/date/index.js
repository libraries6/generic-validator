const chai = require('chai');
const Joi = require('@hapi/joi');
const date = require('./../../src/date');

const { expect } = chai;
let response;

describe('Date validations', () => {
  it('should validate date timestamp', () => {
    response = Joi.validate(new Date(), date.timestamp);
    expect(response.error).be.null;
  });

  it('should not validate string', () => {
    response = Joi.validate('AB1212C', date.timestamp);
    expect(response.error).be.not.null;
  });

  it('should validate iso date', () => {
    response = Joi.validate((new Date()).toISOString(), date.iso);
    expect(response.error).be.null;
  });

  it('should not iso date', () => {
    response = Joi.validate('AB1212C', date.iso);
    expect(response.error).be.not.null;
  });

  it('should validate null as valid iso date', () => {
    response = Joi.validate(null, date.isoWithNull);
    expect(response.error).be.null;
  });
});
